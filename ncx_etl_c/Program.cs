﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;

namespace ncx_etl_c
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                RestClient rc = new RestClient();
                rc.BaseUrl = new Uri("https://bis-appncx01-dev.bis.it.ubc.ca");
                rc.Authenticator = new HttpBasicAuthenticator("restapi", "S1mpl322!");
                rc.ClearHandlers();
                rc.AddHandler("application/json", new RestSharp.Deserializers.JsonDeserializer());
                Console.WriteLine("Rest Client Parameters:");
                foreach (var header in rc.DefaultParameters)
                {
                    Console.WriteLine("{0} : {1}", header.Name, header.Value);
                }

                RestRequest rq = new RestRequest();
                rq.Resource = "rest/devices.json";
                rq.Method = Method.GET;
                rq.RequestFormat = DataFormat.Json;
                rq.AddHeader("AnutaAPIVersion", "1.0");
                rq.AddHeader("Accept", "application/json");
                Console.WriteLine("Request Parameters:");
                foreach (var header in rq.Parameters)
                {
                    Console.WriteLine("{0} : {1}", header.Name, header.Value);
                }

                IRestResponse rp = rc.Execute(rq);
                Console.WriteLine(rp.ContentLength);
                //Console.WriteLine(rp.Content);
                Console.WriteLine("Reponse Code {0}", rp.StatusCode);
                Console.WriteLine("Response Headers:");
                foreach (var header in rp.Headers)
                {
                    Console.WriteLine("{0} : {1}", header.Name, header.Value);
                }

                Console.WriteLine("fin");
                Console.ReadKey();
            }
            catch (SystemException e)
            {
                Console.WriteLine(e);
            }
            
        }
    }
}
